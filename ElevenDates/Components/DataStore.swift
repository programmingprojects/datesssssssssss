//
//  DataStore.swift
//  ElevenDates
//
//  Created by Spencer Sallay on 11/17/15.
//  Copyright © 2015 Brett Keck. All rights reserved.
//

import UIKit

class DataStore: NSObject {
    static let sharedInstance = DataStore()
    
    func getPotential(skip: Int = 0, completion: [DateUser]? -> ()) {
        let currentUser = DateUser.currentUser()!
        let query = DateUser.query()!
        query.skip = skip
        query.whereKey(Constants.objectId, notEqualTo: currentUser.objectId!)
        query.whereKey(Constants.discoverable, equalTo: true)
        switch currentUser.show {
        case .MaleOnly: query.whereKey(Constants.gender, equalTo: Constants.male)
        case .FemaleOnly: query.whereKey(Constants.gender, equalTo: Constants.female)
        case .Both: break
        }
        query.findObjectsInBackgroundWithBlock { (users, error) -> Void in
            completion(users as? [DateUser])
        }
    }
    
    func likePerson(user: DateUser) {
        matchUser(DateUser.currentUser()!,user2: user, isMatch: true)
    }
    
    func nopePerson(user: DateUser) {
        matchUser(DateUser.currentUser()!,user2: user, isMatch: false)
    }
    
    private func matchUser(user1: DateUser, user2: DateUser, isMatch: Bool) {
        let match = DateMatch()
        match.currentUser = user1
        match.targetUser = user2
        match.isMatch = isMatch
        
        checkMatch(match)
    }
    
    private func checkMatch(theMatch: DateMatch) {
        if theMatch.isMatch {
            let matchQuery = DateMatch.query()!
            matchQuery.whereKey(Constants.currentUser, equalTo: theMatch.targetUser)
            matchQuery.whereKey(Constants.targetUser, equalTo: theMatch.currentUser)
            matchQuery.whereKey(Constants.isMatch, equalTo: true)
            matchQuery.getFirstObjectInBackgroundWithBlock({ (match, error) -> Void in
                var mutualMatch = false
                if let foundMatch = match as? DateMatch {
                    mutualMatch = true
                    foundMatch.mutualMatch = mutualMatch
                    foundMatch.saveInBackground()
                }
                self.updateOrInsertMatch(theMatch, mutualMatch: mutualMatch)
            })
        } else {
            updateOrInsertMatch(theMatch, mutualMatch: false)
        }
    }
    
    private func updateOrInsertMatch(theMatch: DateMatch, mutualMatch: Bool) {
        // Create a query on DateMatch
        
        // The query has two clauses -
        // currentUser is the current user
        // targetUser is the match's target user
        // (Use the Constants file for keys)
        
        
        // Get the first object in the background
        // If a match is found, update that record
        // with the proper isMatch and mutualMatch
        // values and save that match
        // If a match is not found, update the mutualMatch
        // value of the theMatch instance
        // and save it.
        if theMatch.isMatch {
            let matchQuery = DateMatch.query()!
            matchQuery.whereKey(Constants.currentUser, equalTo: theMatch.targetUser)
            matchQuery.whereKey(Constants.targetUser, equalTo: theMatch.currentUser)
            matchQuery.getFirstObjectInBackgroundWithBlock({ (match, error) -> Void in
                if let fMatch = match as? DateMatch {
                    fMatch.isMatch = theMatch.isMatch
                    fMatch.mutualMatch = mutualMatch
                    fMatch.saveInBackground()
                } else {
                    theMatch.mutualMatch = mutualMatch
                    theMatch.saveInBackground()
                }
            })
        }
        
    }
}
