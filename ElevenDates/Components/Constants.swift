//
//  Constants.swift
//  ElevenDates
//
//  Created by Brett Keck on 10/23/15.
//  Copyright © 2015 Brett Keck. All rights reserved.
//

import UIKit

struct Constants {
    static let createdAt = "createdAt"
    static let chatRoom = "chatRoom"
    static let sender = "sender"
    static let currentUser = "currentUser"
    static let targetUser = "targetUser"
    static let mutualMatch = "mutualMatch"
    static let male = "male"
    static let female = "female"
    static let dateMatch = "DateMatch"
    static let dateChat = "DateChat"
    static let id = "id"
    static let firstName = "first_name"
    static let lastName = "last_name"
    static let name = "name"
    static let image = "image.jpg"
    static let objectId = "objectId"
    static let gender = "gender"
    static let discoverable = "discoverable"
    static let isMatch = "isMatch"
}

enum Images : String {
    case Closed
    case FailStamp
    case Heart
    case HeartCircle
    case InfoCircle
    case Logo
    case Logout
    case PassStamp
    case Profile
    case Selfie
    case Selfie2
    case XCircle
    
    func image() -> UIImage {
        return UIImage(named: self.rawValue)!
    }
}