//
//  PeopleWatchViewController.swift
//  ElevenDates
//
//  Created by Spencer Sallay on 11/10/15.
//  Copyright © 2015 Brett Keck. All rights reserved.
//

import UIKit

class PeopleWatchViewController: UIViewController, UIGestureRecognizerDelegate {
    @IBOutlet weak var previewUserCardView: UIView!
    @IBOutlet weak var previewUserImage: UIImageView!
    @IBOutlet weak var previewUserNameAge: UILabel!

    @IBOutlet weak var userCardView: UIView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userNameAge: UILabel!
    
    @IBOutlet weak var passImage: UIImageView!
    @IBOutlet weak var failImage: UIImageView!
    
    private var dataStore = DataStore.sharedInstance
    private var cardStartCenter = CGPointZero
    private var potentialMatches: [DateUser]!
    private var skip = 0
    private var nextPreview = 0
    private var currentPotential: DateUser?
    private var nextPotential: DateUser?
    private var actionLock = false

    override func viewDidLoad() {
        super.viewDidLoad()
        loading()
        
        userCardView.layer.cornerRadius = 8.0
        userCardView.clipsToBounds = true
        previewUserCardView.layer.cornerRadius = 8.0
        previewUserCardView.clipsToBounds = true
        let heartButton = UIBarButtonItem(image: Images.Heart.image(), style: .Plain, target: self, action: "goToMatches:")
        let logoutButton = UIBarButtonItem(image: Images.Logout.image(), style: .Plain, target: self, action: "logout:")
        navigationItem.rightBarButtonItems = [logoutButton,heartButton]
        
        let pan = UIPanGestureRecognizer(target: self, action: "moveUserCard:")
        pan.delegate = self
        self.userCardView.addGestureRecognizer(pan)

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        cardStartCenter = CGPoint(x: self.view.frame.size.width / 2.0, y: userCardView.center.y)
        loadCards(true)
    }
    
    // MARK: - private functions
    private func loading() {
        userImage.image = nil
        userNameAge.text = ""
        previewUserImage.image = nil
        previewUserNameAge.text = ""
    }
    
    private func loadCards(reset: Bool) {
        if reset {
            loading()
            skip = 0
        }
        
        dataStore.getPotential(skip) { (potentialReturn) -> () in
            if let potentials = potentialReturn {
                self.skip += potentials.count
                self.potentialMatches = potentials
                self.nextPreview = 0
                if reset {
                    self.firstLoad()
                }
            }
        }
    }

    private func firstLoad() {
        if potentialMatches.isEmpty {
            return
        }
        currentPotential = potentialMatches.first
        if let currentUser = currentPotential {
            userNameAge.text = currentUser.displayText
            currentUser.image.getDataInBackgroundWithBlock({ (data, error) -> Void in
                if error == nil && data != nil {
                    self.userImage.image = UIImage(data: data!)
                }
                })
        }
        
        nextPreview++
        loadPreview()
    }
    
    
    private func loadPreview() {
        if nextPreview >= potentialMatches.count {
            nextPotential = nil
            previewUserImage.image = Images.Closed.image()
            return
        }
        nextPotential = potentialMatches[nextPreview]
        
        if let previewUser = nextPotential {
            previewUserNameAge.text = previewUser.displayText
            previewUser.image.getDataInBackgroundWithBlock({ (data, error) -> Void in
                if error == nil && data != nil {
                    self.previewUserImage.image = UIImage(data: data!)
                }
            })
        }
        if ++nextPreview >= potentialMatches.count {
            loadCards(false)
        }
    }
    
    private func userNextCard() {
        currentPotential = nextPotential
        userImage.image = previewUserImage.image
        userNameAge.text = previewUserNameAge.text
        
        userCardView.center = cardStartCenter
        failImage.alpha = 0x0000
        passImage.alpha = 0x0000
        previewUserImage.image = nil
        previewUserNameAge.text = ""
        loadPreview()
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func doNo() {
        /*1. Whenever you tap a button or swap, actionLock is true.  Then, in doNo/doYes, one of the first things it checks is if actionLock is tue - if true, exit, not let user rate at that point.
          2. Call nopePerson from the dataStore.  Pass in the currentPotential.
          3. Bring up fail image.  Make it visible.
          4. Sned the card off to the left.
          5. Once the app has finished animating the card off-screen, loading new user card and actionLock false...*/
        if currentPotential == nil || actionLock {
            return
        }
        actionLock = true
        dataStore.nopePerson(currentPotential!)
        failImage.alpha = 1
        let offLeft = CGPoint(x: -(self.view.frame.size.width + self.userCardView.frame.size.width), y: 0)
        UIView.animateWithDuration(0.6, animations: { () -> Void in self.userCardView.center = offLeft }) { (finished) -> Void in
            if finished {
                self.userNextCard()
                self.actionLock = false
            }
        }
    }
    
    private func doYes() {
        if currentPotential == nil || actionLock {
            return
        }
        actionLock = true
        dataStore.likePerson(currentPotential!)
        passImage.alpha = 1
        let offRight = CGPoint(x: self.view.frame.size.width + self.userCardView.frame.size.width, y: 0)
        UIView.animateWithDuration(10, animations: { () -> Void in self.userCardView.center = offRight }) { (finished) -> Void in
            if finished {
                self.userNextCard()
                self.actionLock = false
            }
        }
    }

    // MARK: IBActions
    @IBAction func logout(sender: UIBarButtonItem) {
        PFUser.logOut()
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.loginCheck()
    }
    
    @IBAction func goToMatches(sender: UIBarButtonItem) {
        self.performSegueWithIdentifier("MatchesSegue", sender: self)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    /*@IBAction func goToMatches(sender: UIBarButtonItem) {
        
    }*/
    
    @IBAction func noTapped(sender: UIButton) {
        doNo()
    }

    @IBAction func yesTapped(sender: UIButton) {
        doYes()
    }

    @IBAction func infoTapped(sender: UIButton) {

    }
    
    func moveUserCard(sender: UIGestureRecognizer) {
        // Takes a comparable class: minItem < item < maxItem
        func between <T:Comparable> (item: T, minItem: T, maxItem: T) -> T {
            if item > minItem {
                return min(maxItem, item)
            } else {
                return max(minItem, item)
            }
        }
        if currentPotential == nil {
            return
        }
        let panRecognizer = sender as! UIPanGestureRecognizer
        let currentPoint = userCardView.center
        let translation = panRecognizer.translationInView(userCardView.superview!)
        let delta = currentPoint.x - cardStartCenter.x
        let alpha = between(abs(delta), minItem: 0, maxItem: 75.0) / 75.0
        
        switch panRecognizer.state {
        case .Changed:
            userCardView.center = CGPoint(x: currentPoint.x + translation.x, y: currentPoint.y + translation.y)
            if delta > 0 {
                failImage.alpha = 0
                passImage.alpha = alpha
            } else {
                failImage.alpha = alpha
                passImage.alpha = 0
            }
            panRecognizer.setTranslation(CGPointZero, inView: userCardView.superview!)
        case .Ended:
            if alpha < 1.0 {
                UIView.animateWithDuration(0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.8, options: [], animations: { () -> Void in
                    self.userCardView.center = self.cardStartCenter
                    self.failImage.alpha = 0
                    self.passImage.alpha = 0
                }, completion: nil)
            } else {
                if delta > 0 {
                    doYes()
                } else {
                    doNo()
                }
            }
        default:
            break
        }
    }

}
