//
//  ProfileViewController.swift
//  ElevenDates
//
//  Created by Brett Keck on 9/18/15.
//  Copyright © 2015 Brett Keck. All rights reserved.
//

import UIKit

class ProfileViewController: UITableViewController {
    var currentUser = DateUser.currentUser()!
    
    // Mark: Interface builder outlets
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var genderSegment: UISegmentedControl!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var discoverableSwitch: UISwitch!
    @IBOutlet weak var ageSlider: UISlider!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var showText: UILabel!
    
    // Mark: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.interactivePopGestureRecognizer?.enabled = false
        
        //
        userImage.layer.cornerRadius = 8.0
        userImage.clipsToBounds = true
        
        //
        updateUI()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        //
        currentUser.saveInBackground()
    }
    
    func updateUI() {
        //
        currentUser.image.getDataInBackgroundWithBlock { (data, error) -> Void in
            if data != nil && error == nil {
                self.userImage.image = UIImage(data: data!)
            }
        }
        
        //
        name.text = currentUser.firstName
        
        //
        genderSegment.selectedSegmentIndex = currentUser.gender == Constants.male ? 0 : 1
        
        //
        discoverableSwitch.on = currentUser.discoverable
        
        //
        ageLabel.text = "\(currentUser.age)"
        ageSlider.value = Float(currentUser.age)
        
        //
        switch currentUser.show {
        case .MaleOnly:
            showText.text = "Men Only"
        case .FemaleOnly:
            showText.text = "Women Only"
        case .Both:
            showText.text = "Men and Women"
        }
        
    }
    
    // Mark: Actions
    @IBAction func discoverableChanged(sender: UISwitch) {
        currentUser.discoverable = sender.on
    }
    
    @IBAction func ageChanged(sender: UISlider) {
        let age = Int(sender.value)
        currentUser.age = age
        ageLabel.text = "\(age)"
    }
    
    @IBAction func genderChanged(sender: UISegmentedControl) {
        currentUser.gender = sender.selectedSegmentIndex == 0 ? Constants.male : Constants.female
    }
    
    @IBAction func showMenOnly(segue:UIStoryboardSegue) {
        currentUser.show = .MaleOnly
        updateUI()
    }
    
    @IBAction func showWomenOnly(segue:UIStoryboardSegue) {
        currentUser.show = .FemaleOnly
        updateUI()
    }
    
    @IBAction func showBoth(segue:UIStoryboardSegue) {
        currentUser.show = .Both
        updateUI()
    }
}