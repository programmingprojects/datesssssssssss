//
//  MatchTableViewController.swift
//  ElevenDates
//
//  Created by Spencer Sallay on 11/30/15.
//  Copyright © 2015 Brett Keck. All rights reserved.
//

import UIKit
import ParseUI

class MatchTableViewController: PFQueryTableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func queryForTable() -> PFQuery {
        let query = DateMatch.query()!
        query.whereKey(Constants.currentUser, equalTo: DateUser.currentUser()!)
        query.whereKey(Constants.mutualMatch, equalTo: true)
        query.includeKey(Constants.targetUser)
        return query
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath, object: PFObject?) -> PFTableViewCell? {
        var cell = tableView.dequeueReusableCellWithIdentifier("PFTableViewController") as? PFTableViewCell
        if cell == nil {
            cell = PFTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "PFTableViewCell")
            cell?.imageView?.layer.cornerRadius = 0.0
            cell?.imageView?.clipsToBounds = true
            cell?.imageView?.contentMode = UIViewContentMode.ScaleAspectFit
        }
        let match = object as! DateMatch
        cell?.textLabel?.text = match.targetUser.displayText
        cell?.imageView?.file = match.targetUser.image
        return cell!
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row < self.objects?.count {
            let match = objectAtIndexPath(indexPath) as! DateMatch
            let chatVC = ChatViewController()
            chatVC.currentUser = DateUser.currentUser()
            chatVC.otherUser = match.targetUser
            self.navigationController?.pushViewController(chatVC, animated: true)
        } else { self.loadNextPage() }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
