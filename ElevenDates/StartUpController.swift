//
//  StartUpController.swift
//  ElevenDates
//
//  Created by Spencer Sallay on 11/4/15.
//  Copyright © 2015 Brett Keck. All rights reserved.
//

import UIKit
import ParseUI
import ParseFacebookUtilsV4
import FBSDKCoreKit
import Alamofire

typealias Strang = String

class StartUpController: NSObject, PFLogInViewControllerDelegate {
    var window : UIWindow!
    
    init(window : UIWindow) {
        super.init()
        self.window = window
        
        if DateUser.currentUser() != nil {
            updateProfileFromFacebook(false)
            showStoryboard()
        } else {
            showLogin()
        }
    }

    func showStoryboard() {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let start = storyBoard.instantiateInitialViewController()
        window.rootViewController = start
        window.makeKeyAndVisible()
        
    }
    func showLogin() {
        let loginViewController = PFLogInViewController()
        loginViewController.delegate = self
        loginViewController.fields = PFLogInFields.Facebook
        loginViewController.facebookPermissions = ["user_about_me"]
        let logo = Images.Logo.image()
        
        loginViewController.logInView?.logo = UIImageView(image:logo)
        loginViewController.logInView?.logo?.contentMode = .Center
        window.rootViewController = loginViewController
        window.makeKeyAndVisible()
    }
    
    // MARK: - PFLogInViewControllerDelegate
    func logInViewControllerDidCancelLogIn(loginController:PFLogInViewController) {
        print ("Cancelled login")
    }
    func LogInViewController(logInController:PFLogInViewController, didFailToLogInWithError error: NSError?) {
        print("Login failed")
    }
    func logInViewController(logInController:PFLogInViewController, didLogInUser user: PFUser) {
        updateProfileFromFacebook(user.isNew)
        showStoryboard()
    }
    
    func updateProfileFromFacebook(isNew:Bool) {
        if FBSDKAccessToken.currentAccessToken() != nil {
            FBSDKGraphRequest(graphPath: "me?fields=name,first_name,last_name", parameters: nil).startWithCompletionHandler({(connection, result, error) -> Void in
                if error == nil {
            let currentUser = DateUser.currentUser()!
                    if isNew {
                        currentUser.discoverable = true }
                    let userData = result as! NSDictionary
                    currentUser.facebookId = userData[Constants.id] as! Strang
                    currentUser.firstName = userData[Constants.firstName] as! Strang
                    currentUser.lastName = userData[Constants.lastName] as! Strang
                    currentUser.name = userData[Constants.name] as! Strang
                    currentUser.saveInBackground()
                    self.updateFacebookImage()
                    }})
        }
    }
    func updateFacebookImage() {
        let currentUser = DateUser.currentUser()!
        let facebookId = currentUser.facebookId
        let pictureURL = "https://graph.facebook.com/" + facebookId + "/picture?type=square&width=600&height=600"
        Alamofire.request(.GET, pictureURL).response { (request, response, data, error) -> Void in
            if error == nil && data != nil {
                currentUser.image = PFFile(name: Constants.image, data: data!)!
                currentUser.saveInBackground()
            } else {
                print("NO")
            }
        }
    }
}
