//
//  DateUser.swift
//  ElevenDates
//
//  Created by Spencer Sallay on 11/4/15.
//  Copyright © 2015 Brett Keck. All rights reserved.
//

import UIKit

class DateUser: PFUser {
    @NSManaged var facebookId : String
    @NSManaged var firstName : String
    @NSManaged var name : String
    @NSManaged var lastName: String
    @NSManaged var image : PFFile
    @NSManaged var discoverable : Bool
    
    var gender : String {
        get {
            return self[Constants.gender] as? String ?? Constants.male
        }
        set {
            self[Constants.gender] = newValue
        }
    }
    
    var age : Int {
        get {
            return self["age"] as? Int ?? 25
        }
        set {
            self["age"] = newValue
        }
    }
    
    var show : DatePreference {
        get {
            return DatePreference(rawValue: self["show"] as? Int ?? 3)!
        }
        set {
            self["show"] = newValue.rawValue
        }
    }
    
    var displayText : String {
        return "\(firstName), \(age)"
    }
    
}

@objc enum DatePreference: Int {
    case MaleOnly = 1
    case FemaleOnly = 2
    case Both = 3
}
