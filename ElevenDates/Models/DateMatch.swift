//
//  DateMatch.swift
//  ElevenDates
//
//  Created by Spencer Sallay on 11/18/15.
//  Copyright © 2015 Brett Keck. All rights reserved.
//

import UIKit

class DateMatch: PFObject, PFSubclassing {
    static func parseClassName() -> String {
        return Constants.dateMatch
    }
    
    @NSManaged var currentUser: DateUser
    @NSManaged var targetUser: DateUser
    @NSManaged var isMatch: Bool
    @NSManaged var mutualMatch: Bool
    
}
